import zeep
from zeep import Client
import web

# Generating routes
urls = (
    '/', 'none',
    '/([A-Za-z]{2})', 'country',
    '/.*', 'notvalid'
)
app = web.application(urls, globals())

wsdl = 'http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL'
client = zeep.Client(wsdl)

class country:
    def GET(self, name):
        name = name.upper()
        request = client.service.FullCountryInfo(name)
        output = str(request['sName']) + '\nCode : ' + str(request['sISOCode']) + '\nCapitale : ' + str(request['sCapitalCity']) + '\nDevise : ' + str(request['sCurrencyISOCode'])
        return output

class notvalid:
    def GET(self):
        return 'Veuillez saisir un ISO Code correct'

class none:
    def GET(self):
        return 'Veuillez saisir l\'ISO Code d\'un pays'

if __name__ == "__main__":
    app.run()
